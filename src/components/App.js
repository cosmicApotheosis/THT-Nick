import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';

import { fetchExchangeRatesBTC, fetchExchangeRatesETH } from '../actions';

class App extends Component {
  /**
   * @method constructor
   * @param {object} props - Component props
   * @returns {undefined}
   */
  constructor(props) {
    super(props);

    this.state = {
      currencyUnit: 'USD',
      fetchInterval: 60,
      completed: '',
      lastBTCRates: {},
      lastETHRates: {}
    }

    // display units dropdown is controlled
    this.handleUnitChange = this.handleUnitChange.bind(this);
    
    // fetch interval input is uncontrolled
    this.fetchIntervalInputValue = React.createRef();
    this.handleIntervalSubmit = this.handleIntervalSubmit.bind(this);
  }

  timer = null;
  /**
   * @method componentDidMount
   * @returns {undefined}
   */
  componentWillMount() {
    // countdown until next API call
    this.timer = setInterval(this.progress, 1000);
  }
  /**
   * @method componentWillUnmount
   * @returns {undefined}
   */
  componentWillUnmount() {
    clearInterval(this.timer);
  }
  /**
   * Decrements API call timer
   * @method progress
   * @returns {undefined}
   */
  progress = () => {
    const { completed, fetchInterval } = this.state;
    const { btcExchangeRates, ethExchangeRates, 
      fetchExchangeRatesBTC, fetchExchangeRatesETH } = this.props;

    if (completed === 0 || !completed) {
      // reset counter
      this.setState({ completed: fetchInterval });

      // save current exchange rate in component state
      this.setState({ lastBTCRates: btcExchangeRates });
      this.setState({ lastETHRates: ethExchangeRates });

      // fetch data here
      fetchExchangeRatesBTC();
      fetchExchangeRatesETH();
    } else {
      // decrement counter by 1
      const diff = -1;
      this.setState({ completed: Math.max(completed + diff, 0) });
    }
  };
  /**
   * Updates component state when user selects new display currency
   * @method handleUnitChange
   * @param {Event} event 
   * @returns {undefined}
   */
  handleUnitChange(event) {
    // set display currency unit
    this.setState({ currencyUnit: event.target.value });
  }
  /**
   * Resets API call timer to new value
   * @method handleIntervalSubmit
   * @param {Event} event 
   * @returns {undefined}
   */
  handleIntervalSubmit(event) {
    event.preventDefault();
    // if string input isn't converted to number, counter doesnt work
    this.setState({ fetchInterval: Number(this.fetchIntervalInputValue.current.value) });
    // stop timer
    clearInterval(this.timer);
    // set count to 0
    this.setState({ completed: '' });
    // reset timer. with count set to 0, API calls are made
    this.timer = setInterval(this.progress, 1000);
  }

  render() {

    const btcPrice = this.props.btcExchangeRates[this.state.currencyUnit];
    const lastBTCPrice = this.state.lastBTCRates[this.state.currencyUnit];
    const ethPrice = this.props.ethExchangeRates[this.state.currencyUnit];
    const lastETHPrice = this.state.lastETHRates[this.state.currencyUnit];

    const { currencyUnit, lastBTCRates, lastETHRates, 
      completed } = this.state;

    // check for errors in API data
    if (this.props.btcExchangeRates.error || this.props.ethExchangeRates.error) {
      return (
        <div className="container">
          <br />
          API error...
        </div>
      );
    }

    // only if the exchange rates state objects are populated display the prices
    if (!_.isEmpty(this.props.btcExchangeRates) && !_.isEmpty(this.props.ethExchangeRates)) {
      return (
        <div className="container">
          <br />
          <h1>FE Developer Take Home Assignment</h1>
          <br />

          {/* allow user to set refresh interval */}
          <form onSubmit={this.handleIntervalSubmit}>
          <div className="form-group">
            {/* select dropdown values taken from API response for currency unit */}
              <label for="units">Specify display unit</label>
              <select id="units" className="form-control" value={currencyUnit} onChange={this.handleUnitChange}>
                {_.keys(this.props.btcExchangeRates).map(unit => {
                  return (
                    <option value={unit} key={unit}>{unit}</option>
                  );
                })}
              </select>
            </div>
            <div className="form-group">
              <label for="interval">Fetch interval (s)</label>
              <input id="interval" className="form-control" type="text" ref={this.fetchIntervalInputValue} placeholder="60" />
            </div>
            <button type="submit" className="btn btn-default">Update</button>
          </form>
          
          <br />
          {/* display countdown until next API call */}
          <h4>Next update in {completed} s</h4>
          <br />

          {/* display 0% change if previous price isn't yet saved to state */}
          {/* beware of javascript rounding behavior */}
          <h2>
            BTC: {btcPrice} ({!_.isEmpty(lastBTCRates) ?
              (btcPrice - lastBTCPrice) / lastBTCPrice : 0}%)
            <br />
            ETH: {ethPrice} ({!_.isEmpty(lastETHRates) ?
              (ethPrice - lastETHPrice) / lastETHPrice : 0}%)
          </h2>
        </div>
      )
    }

    return (
      <div className="container">
        <br />
        Fetching API data...
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    btcExchangeRates: state.btcExchangeRates,
    ethExchangeRates: state.ethExchangeRates
  };
}

App.propTypes = {
  btcExchangeRates: PropTypes.object.isRequired,
  ethExchangeRates: PropTypes.object.isRequired,
  fetchExchangeRatesBTC: PropTypes.func.isRequired,
  fetchExchangeRatesETH: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, { fetchExchangeRatesBTC, fetchExchangeRatesETH })(App);
