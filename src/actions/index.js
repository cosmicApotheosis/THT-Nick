import axios from 'axios';

import { FETCH_EXCHANGE_RATES_BTC, FETCH_EXCHANGE_RATES_ETH } from './types';

// https://api.coinbase.com/v2/exchange-rates?currency=BTC
// https://api.coinbase.com/v2/exchange-rates?currency=ETH
const ROOT_URL = 'https://api.coinbase.com/v2/exchange-rates?currency=';
/**
 * Retrieves BTC exchange rates from coinbase API
 * @function fetchExchangeRatesBTC
 * @returns {object}
 */
export function fetchExchangeRatesBTC() {
  const request = axios.get(`${ROOT_URL}BTC`);

  return {
    type: FETCH_EXCHANGE_RATES_BTC,
    payload: request
  }
}
/**
 * Retrieves ETH exchange rates from coinbase API
 * @function fetchExchangeRatesETH
 * @returns {object}
 */
export function fetchExchangeRatesETH() {
  const request = axios.get(`${ROOT_URL}ETH`);

  return {
    type: FETCH_EXCHANGE_RATES_ETH,
    payload: request
  }
}

