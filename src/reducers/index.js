import { combineReducers } from 'redux';
import btcExchangeRatesReducer from './btcExchangeRatesReducer';
import ethExchangeRatesReducer from './ethExchangeRatesReducer';

const rootReducer = combineReducers({
    btcExchangeRates: btcExchangeRatesReducer,
    ethExchangeRates: ethExchangeRatesReducer
});

export default rootReducer;
