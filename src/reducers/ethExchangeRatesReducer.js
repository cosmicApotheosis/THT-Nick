import { FETCH_EXCHANGE_RATES_ETH } from '../actions/types';
/**
 * @function ethExchangeRatesReducer
 * @param {string} state - State before reducer.
 * @param {object} action - Action sent to reducer.
 * @returns {object} - New state (object of exchange rates).
 */
export default function(state = {}, action) {
  // rudimentary error handling since handling errors
  // with axios+redux-promise isn't built in
  if (action.error) {
    return state;
  }
  switch (action.type) {
    case FETCH_EXCHANGE_RATES_ETH:
      return action.payload.data.data.rates;
    default:
      return state;
  }
}